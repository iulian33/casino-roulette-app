import { Store } from "../common/models/Store";

export const getGameData = (store: Store) => store.gameData;
export const getChipsData = (store: Store) => store.chipsData;
