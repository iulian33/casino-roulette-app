import { GameStages } from "../../common/constants";
import { createSlice } from "@reduxjs/toolkit";
import { GameData } from "../../common/models/Game";
import {
  GameBetsConfirmedPayload,
  GameHistoryPayload,
  GameStagePayload,
  GameUsernamePayload,
  GameWinnersPayload,
  GameWinNumberPayload
} from "../actionsTypes";

const initialState: GameData = {
  wheelWinNumber: 0,
  winners: [],
  history: [],
  betsConfirmed: false,
  stage: GameStages.NONE,
  username: ""
};

const gameDataSlice = createSlice({
  name: "gameData",
  initialState: initialState,
  reducers: {
    updateGameStage(state: GameData, { payload }: GameStagePayload) {
      return {
        ...state,
        stage: payload
      };
    },
    updateGameHistory(state: GameData, { payload }: GameHistoryPayload): GameData {
      return {
        ...state,
        history: payload
      };
    },
    updateWinNumber(state, { payload }: GameWinNumberPayload): GameData {
      return {
        ...state,
        wheelWinNumber: payload
      };
    },
    updateWinnersBoard(state, { payload }: GameWinnersPayload): GameData {
      return {
        ...state,
        winners: payload
      };
    },
    setBetsConfirmed(state, { payload }: GameBetsConfirmedPayload): GameData {
      return {
        ...state,
        betsConfirmed: payload
      };
    },
    setUsername(state, { payload }: GameUsernamePayload): GameData {
      return {
        ...state,
        username: payload
      };
    }
  }
});

export const {
  updateGameStage,
  updateGameHistory,
  updateWinNumber,
  updateWinnersBoard,
  setBetsConfirmed,
  setUsername
} = gameDataSlice.actions;
export default gameDataSlice.reducer;
