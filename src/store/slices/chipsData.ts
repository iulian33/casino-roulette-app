import { createSlice } from "@reduxjs/toolkit";
import { ChipsData, PlacedChip } from "../../common/models/Chip";
import { CellItem } from "../../common/models/Game";
import { ChipPlacedPayload, ChipSelectedPayload } from "../actionsTypes";

const initialState: ChipsData = {
  selectedChip: null,
  placedChips: new Map<CellItem, PlacedChip>()
};

const chipsDataSlice = createSlice({
  name: "chipsData",
  initialState: initialState,
  reducers: {
    updateSelectedChip(state, { payload }: ChipSelectedPayload) {
      return {
        ...state,
        selectedChip: payload
      };
    },
    updatePlacedChips(state, { payload }: ChipPlacedPayload) {
      if (state.selectedChip === 0 || state.selectedChip === null) {
        return state;
      }
      let currentChip = {} as PlacedChip;
      currentChip.item = payload;
      currentChip.sum = state.selectedChip;

      if (state.placedChips.get(payload) !== undefined) {
        currentChip.sum += state.placedChips.get(payload)!.sum;
      }

      state.placedChips.set(payload, currentChip);
    },
    clearChips(state) {
      state.placedChips = new Map();
    }
  }
});

export const { updateSelectedChip, updatePlacedChips, clearChips } = chipsDataSlice.actions;
export default chipsDataSlice.reducer;
