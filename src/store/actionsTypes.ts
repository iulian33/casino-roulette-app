import { GameStages } from "../common/constants";
import { CellItem, Winner } from "../common/models/Game";

export type GameStagePayload = { payload: GameStages };
export type GameHistoryPayload = { payload: number[] };
export type GameWinNumberPayload = { payload: number };
export type GameWinnersPayload = { payload: Winner[] };
export type GameBetsConfirmedPayload = { payload: boolean };
export type GameUsernamePayload = { payload: string };
export type ChipSelectedPayload = { payload: number };
export type ChipPlacedPayload = { payload: CellItem };
