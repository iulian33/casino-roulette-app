import { configureStore } from "@reduxjs/toolkit";
import gameDataReducer from "./slices/gameData";
import chipsDataReducer from "./slices/chipsData";
import { enableMapSet } from "immer";

enableMapSet();
export const store = configureStore({
  devTools: {
    serialize: {
      options: {
        map: true
      }
    }
  },
  reducer: {
    gameData: gameDataReducer,
    chipsData: chipsDataReducer
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false
    })
});
