import React, { useRef, useState } from "react";
import { createRoot } from "react-dom/client";
import { Button, InputAdornment, styled, TextField } from "@mui/material";
import { GameWrapper } from "./common/components/GameWrapper";
import { AccountCircle } from "@mui/icons-material";
import { Provider } from "react-redux";
import { store } from "./store";
import "./styles.css";

const rootElement = document.getElementById("app") as HTMLElement;
const root = createRoot(rootElement);

function App() {
  const nameRef = useRef("");
  const [usernameValue, setUsernameValue] = useState("");

  const LoginInput = styled(TextField)({
    "& .Mui-focused": {
      color: "#fff !important"
    },
    "& .MuiInputBase-root": {
      padding: "10px 0 !important",
      "&:after": {
        borderBottom: "2px solid #fff !important"
      }
    }
  });

  return (
    <Provider store={store}>
      {!usernameValue && (
        <div className="lobby-room">
          <LoginInput
            fullWidth
            id="playerName"
            label="Player Name"
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              nameRef.current = event.target.value;
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <AccountCircle />
                </InputAdornment>
              )
            }}
            variant="standard"
          />

          <Button
            variant="contained"
            className="login-button"
            fullWidth
            color="success"
            onClick={() => {
              nameRef.current.length >= 2 && setUsernameValue(nameRef.current);
            }}
            size="large"
          >
            Enter Game
          </Button>
        </div>
      )}
      {!!usernameValue && <GameWrapper username={usernameValue} />}
    </Provider>
  );
}
export default App;
root.render(<App />);
