import anime from "animejs";
import { RotationDegree, RouletteWheelNumbers, TotalGameNumbers } from "../constants";

export const getRandomEndRotation = (minNumberOfSpins: number, maxNumberOfSpins: number) =>
  RotationDegree *
  anime.random(minNumberOfSpins * TotalGameNumbers, maxNumberOfSpins * TotalGameNumbers);
export const getRouletteIndexFromNumber = (number: number) => RouletteWheelNumbers.indexOf(number);

export const getRotationFromNumber = (number: number) =>
  RotationDegree * getRouletteIndexFromNumber(number);

export const getBallEndRotation = (zeroEndRotation: number, currentNumber: number) =>
  Math.abs(zeroEndRotation) + getRotationFromNumber(currentNumber);

export const getBallNumberOfRotations = (minNumberOfSpins: number, maxNumberOfSpins: number) =>
  360 * anime.random(minNumberOfSpins, maxNumberOfSpins);

export const getZeroEndRotation = (totalRotation: number) => 360 - Math.abs(totalRotation % 360);
