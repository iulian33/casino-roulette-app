import { CellItem } from "../models/Game";
import { ValueType } from "../constants";

export const getNumbersList = () => {
  const colList: CellItem[][] = [];
  const difference = 3;

  for (let i = 1; i <= 5; i++) {
    const rowList: Partial<CellItem>[] = [];

    if (i === 1) {
      rowList.push({ type: ValueType.NUMBER, value: 0 });
    }

    for (let j = 1; j <= 26; j++) {
      if (j > 24) {
        rowList.push({ type: ValueType.EMPTY });
        continue;
      }

      const isEvenRow = i % 2 === 0;
      const isEvenColumn = j % 2 === 0;
      const isQuadSplit = isEvenRow && !isEvenColumn;
      const isDoubleSplit = (isEvenRow && isEvenColumn) || (!isEvenRow && isEvenColumn);
      const isTripleSplit = !isEvenRow && !isEvenColumn;

      const startNumberSub = i === 3 ? 1 : i === 5 ? 2 : 0;
      const prevStartNumberSub = i - 1 === 3 ? 1 : i - 1 === 5 ? 2 : 0;
      const nextStartNumberSub = i + 1 === 3 ? 1 : i + 1 === 5 ? 2 : 0;

      if (isQuadSplit) {
        const leftNumber = ((j - 1) / 2) * difference - prevStartNumberSub;
        const rightNumber = leftNumber + difference;
        const bottomLeftNumber = ((j - 1) / 2) * difference - nextStartNumberSub;
        const bottomRightNumber = bottomLeftNumber + difference;

        rowList.push({
          type: ValueType.QUAD_SPLIT,
          valueSplit: [leftNumber, rightNumber, bottomLeftNumber, bottomRightNumber]
        });
      } else if (isDoubleSplit) {
        if (j === 1) {
          const leftNumber = 0;
          const rightNumber = leftNumber + difference;

          rowList.push({ type: ValueType.DOUBLE_SPLIT, valueSplit: [leftNumber, rightNumber] });
        } else {
          const currentNumber = ((j - 2) / 2) * difference + difference - startNumberSub;

          rowList.push({ type: ValueType.NUMBER, value: currentNumber });
        }
      } else if (isTripleSplit) {
        const leftNumber = 0;
        const topNumber = difference - prevStartNumberSub;
        const bottomNumber = difference - nextStartNumberSub;

        rowList.push({
          type: ValueType.TRIPLE_SPLIT,
          valueSplit: [leftNumber, topNumber, bottomNumber]
        });
      }
    }

    colList.push(rowList as CellItem[]);
  }

  return colList;
};
