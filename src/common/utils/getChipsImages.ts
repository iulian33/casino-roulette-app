import { ChipImage, PlacedChip } from "../models/Chip";
import { ChipValues } from "../constants";
import { getChipClasses } from "./getChipClasses";
import { CellItem } from "../models/Game";

export const getChipsImages = (
  chipData: Readonly<PlacedChip>,
  currentItem: CellItem
): ChipImage[] => {
  const chipsImages: ChipImage[] = [];
  let tempSum = chipData.sum;

  for (const chipValue of ChipValues) {
    let chipsToPlace = Math.min(Math.floor(tempSum / chipValue), 99);
    let totalValuePlaced = chipsToPlace * chipValue;

    for (let i = 0; i < chipsToPlace; i++) {
      chipsImages.push({
        key: `${currentItem.type}_${currentItem.value}_${chipValue}_${i}`,
        classes: getChipClasses(chipValue, true)
      });
    }

    tempSum -= totalValuePlaced;
  }

  return chipsImages;
};
