import { RouletteWheelNumbers } from "../constants";

export const getRouletteColor = (number: number) => {
  let index = RouletteWheelNumbers.indexOf(number);
  const i = index >= 0 ? index % 37 : 37 - Math.abs(index % 37);
  return i === 0 || number === null ? "none" : i % 2 === 0 ? "black" : "red";
};
