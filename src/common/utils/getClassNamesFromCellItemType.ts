import classNames from "classnames";
import { ValueType } from "../constants";
import { getRouletteColor } from "./getRouletteColor";

export const getClassNamesFromCellItemType = (type: ValueType, number: number | null) => {
  let isEvenOdd = 0;
  if (number != null && type === ValueType.NUMBER && number !== 0) {
    if (number % 2 === 0) {
      isEvenOdd = 1;
    } else {
      isEvenOdd = 2;
    }
  }
  return classNames({
    "board-cell-number": type === ValueType.NUMBER,
    "board-cell-double-split": type === ValueType.DOUBLE_SPLIT,
    "board-cell-quad-split": type === ValueType.QUAD_SPLIT,
    "board-cell-triple-split": type === ValueType.TRIPLE_SPLIT,
    "board-cell-empty": type === ValueType.EMPTY,
    "board-cell-even": type === ValueType.EVEN || isEvenOdd === 1,
    "board-cell-odd": type === ValueType.ODD || isEvenOdd === 2,
    "board-cell-number-1-18":
      type === ValueType.NUMBERS_1_18 ||
      (number !== null && number >= 1 && number <= 18 && type === ValueType.NUMBER),
    "board-cell-number-19-36":
      type === ValueType.NUMBERS_19_36 ||
      (number !== null && number >= 19 && number <= 36 && type === ValueType.NUMBER),
    "board-cell-number-1-12":
      type === ValueType.NUMBERS_1_12 ||
      (number !== null && number % 3 === 0 && type === ValueType.NUMBER && number !== 0),
    "board-cell-number-2-12":
      type === ValueType.NUMBERS_2_12 ||
      (number !== null && number % 3 === 2 && type === ValueType.NUMBER),
    "board-cell-number-3-12":
      type === ValueType.NUMBERS_3_12 ||
      (number !== null && number % 3 === 1 && type === ValueType.NUMBER),
    "board-cell-red":
      type === ValueType.RED ||
      (number !== null && getRouletteColor(number) === "red" && type === ValueType.NUMBER),
    "board-cell-black":
      type === ValueType.BLACK ||
      (number !== null && getRouletteColor(number) === "black" && type === ValueType.NUMBER)
  });
};
