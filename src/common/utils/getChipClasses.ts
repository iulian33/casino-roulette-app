export const getChipClasses = (chipValue: number, isPlaced: boolean) => {
  const baseClass = `chip-${chipValue}`;
  const placedClass = isPlaced ? `${baseClass}-placed` : "";
  return `${baseClass} chipValueImage ${placedClass}`;
};
