import { CellItem } from "./Game";

export type ChipsData = {
  selectedChip: number | null;
  placedChips: Map<Partial<CellItem> | null, PlacedChip>;
};
export interface PlacedChip {
  item: CellItem;
  sum: number;
}

export interface ChipImage {
  key: string;
  classes: string;
}
