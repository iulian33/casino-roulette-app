import { GameStages, ValueType } from "../constants";

export interface CellItem {
  type: ValueType;
  value: number;
  valueSplit?: number[];
}

export type GameData = {
  wheelWinNumber: number;
  winners: Winner[];
  betsConfirmed: boolean;
  username: string;
  stage: GameStages;
  history: number[];
};

export type Winner = {
  username: string;
  sum: number;
};

export type ServerGameData = {
  stage: GameStages;
  rouletteWinValue: number;
  wins: Winner[];
  history: number[];
};
