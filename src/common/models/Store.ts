import { GameData } from "./Game";
import { ChipsData } from "./Chip";

export interface Store {
  gameData: GameData;
  chipsData: ChipsData;
}
