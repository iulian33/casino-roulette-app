import React, { useCallback, useEffect, useState } from "react";
import { GameStages } from "../../constants";
import WinnersBoard from "../RouletteHeading/WinnersBoard";
import WinningsHistory from "../RouletteHeading/WinningsHistory";
import { Box, Grid, styled } from "@mui/material";
import { ServerGameData } from "../../models/Game";
import { io, Socket } from "socket.io-client";
import { BoardGame } from "../BoardGame";
import { StageProgress } from "../StageProgress";
import { useDispatch } from "react-redux";
import {
  setUsername,
  updateGameHistory,
  updateGameStage,
  updateWinnersBoard,
  updateWinNumber
} from "../../../store/slices/gameData";
import BettingActions from "../BettingActions";
import { Wheel } from "../RouletteHeading/Wheel";
import { GameMessage } from "../GameMessage";

interface GameWrapperProps {
  username: string;
}

let socketServer: Socket | null = null;

export const GameWrapper = ({ username }: GameWrapperProps) => {
  const dispatch = useDispatch();
  const [socketRef, setSocketRef] = useState<Socket | null>();

  const setGameData = useCallback(
    (gameData: ServerGameData) => {
      switch (gameData.stage) {
        case GameStages.NO_MORE_BETS:
          dispatch(updateWinNumber(gameData.rouletteWinValue));
          break;
        case GameStages.WINNERS:
          if (gameData.wins.length > 0) {
            dispatch(updateWinnersBoard(gameData.wins));
            dispatch(updateGameHistory(gameData.history));
          } else {
            dispatch(updateGameHistory(gameData.history));
          }
      }
      dispatch(updateGameStage(gameData.stage));
    },
    [dispatch]
  );

  useEffect(() => {
    socketServer = io("http://localhost:8000");
  }, []);

  useEffect(() => {
    socketServer?.open();
    socketServer?.on("connect", () => {
      socketServer?.emit("enter", username);
      dispatch(setUsername(username));
      setSocketRef(socketServer);
    });

    socketServer?.on("game-update", (data: string) => {
      let gameData: ServerGameData = JSON.parse(data);
      setGameData(gameData);
    });

    return () => {
      socketServer?.close();
    };
  }, [setGameData, username, dispatch]);

  const ProgressContainer = styled(Grid)({
    position: "absolute",
    bottom: 0,
    textAlign: "center",
    zIndex: -1
  });

  return (
    <>
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <WinnersBoard />
          </Grid>
          <Grid item xs={4}>
            <Wheel />
          </Grid>
          <Grid item xs={4}>
            <WinningsHistory />
          </Grid>
        </Grid>
      </Box>

      <GameMessage />
      <BoardGame />
      <BettingActions socketServer={socketRef} />

      <ProgressContainer container spacing={3}>
        <Grid item xs={4}>
          <StageProgress />
        </Grid>
      </ProgressContainer>
    </>
  );
};
