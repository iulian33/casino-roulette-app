import styled from "styled-components";

export const StageProgressContainer = styled.div`
  position: relative;
  margin: 0 auto 80px;
  padding-right: 20px;
`;
