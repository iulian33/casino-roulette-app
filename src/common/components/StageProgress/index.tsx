import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { GameStages } from "../../constants";
import { CircularProgress } from "@mui/material";
import { getGameData } from "../../../store/selectors";
import { StageProgressContainer } from "./styled";

let timer = {} as NodeJS.Timer;
export const StageProgress = () => {
  const [stageTimer, setStageTimer] = useState(0);
  const [progress, setProgress] = useState(0);
  const { stage } = useSelector(getGameData);

  useEffect(() => {
    setProgress(0);
    switch (stage) {
      case GameStages.PLACE_BET:
        setStageTimer(19000);
        break;
      case GameStages.NO_MORE_BETS:
        setStageTimer(4700);
        break;
      case GameStages.WINNERS:
        setStageTimer(3700);
        break;
    }
  }, [stage]);

  useEffect(() => {
    clearInterval(timer);
    timer = setInterval(() => {
      setProgress(prevProgress => prevProgress + 10000 / stageTimer);
    }, 100);

    return () => {
      clearInterval(timer);
    };
  }, [stageTimer]);

  return (
    <StageProgressContainer>
      <CircularProgress size="10rem" variant="determinate" value={progress} />
    </StageProgressContainer>
  );
};
