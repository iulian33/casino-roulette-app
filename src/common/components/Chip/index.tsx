import React from "react";
import { getChipsImages } from "../../utils/getChipsImages";
import { PlacedChip } from "../../models/Chip";
import { CellItem } from "../../models/Game";

type ChipProps = { currentItemChips: PlacedChip; currentItem: CellItem };

export const Chip = ({ currentItemChips, currentItem }: ChipProps) => (
  <>
    {getChipsImages(currentItemChips, currentItem).map(item => (
      <div key={item.key} className={item.classes} />
    ))}
  </>
);
