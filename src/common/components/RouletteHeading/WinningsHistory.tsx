import React from "react";
import { useSelector } from "react-redux";
import { getGameData } from "../../../store/selectors";
import { History, HistoryItem, WinHistoryContainer } from "./styled";

const WinningsHistory = () => {
  const { history } = useSelector(getGameData);

  return (
    <WinHistoryContainer>
      <History>
        {history.map((item: number) => (
          <HistoryItem item={item}>{item}</HistoryItem>
        ))}
      </History>
    </WinHistoryContainer>
  );
};

export default WinningsHistory;
