import anime from "animejs";
import React, { useCallback, useEffect, useRef } from "react";
import { SpinAnimationBezier, SpinAnimationDuration } from "../../constants";
import { useSelector } from "react-redux";
import { getGameData } from "../../../store/selectors";
import {
  getBallEndRotation,
  getBallNumberOfRotations,
  getRandomEndRotation,
  getRotationFromNumber,
  getZeroEndRotation
} from "../../utils/wheelHelpers";
import { Layer, RouletteWheel, WheelBall } from "./styled";

export const Wheel = () => {
  const lastNumber = useRef(0);
  const { wheelWinNumber } = useSelector(getGameData);

  const spinWheel = useCallback(
    (number: number) => {
      const endRotation = -getRandomEndRotation(2, 4);
      const ballEndRotation =
        getBallNumberOfRotations(2, 4) +
        getBallEndRotation(getZeroEndRotation(endRotation), number);

      anime.set(["#layer-2", "#layer-4"], {
        rotate: getRotationFromNumber(lastNumber.current)
      });

      anime.set(".ball-container", { rotate: 0 });

      anime({
        targets: ["#layer-2", "#layer-4"],
        rotate: endRotation,
        duration: SpinAnimationDuration,
        easing: `cubicBezier(${SpinAnimationBezier.join(",")})`,
        complete: () => {
          lastNumber.current = number;
        }
      });

      anime({
        targets: "#ball-container",
        translateY: [
          { value: 0, duration: 2000 },
          { value: 20, duration: 1000 },
          { value: 25, duration: 900 },
          { value: 50, duration: 1000 }
        ],
        rotate: [{ value: ballEndRotation, duration: SpinAnimationDuration }],
        loop: 1,
        easing: `cubicBezier(${SpinAnimationBezier.join(",")})`
      });
    },
    [getBallEndRotation, getRotationFromNumber, getRandomEndRotation]
  );

  useEffect(() => {
    if (wheelWinNumber !== null) spinWheel(wheelWinNumber);
  }, [wheelWinNumber, spinWheel]);

  return (
    <RouletteWheel>
      <Layer idx={2} isWheel id="layer-2" style={{ transform: "rotate(0deg)" }} />
      <Layer idx={3} />
      <Layer idx={4} isWheel id="layer-4" style={{ transform: "rotate(0deg)" }} />
      <Layer idx={5} />
      <Layer idx={0} isWheel id="ball-container" style={{ transform: "rotate(0deg)" }}>
        <WheelBall style={{ transform: "translate(0, -163.221px)" }} />
      </Layer>
    </RouletteWheel>
  );
};
