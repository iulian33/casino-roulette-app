import React from "react";
import { useSelector } from "react-redux";
import { getGameData } from "../../../store/selectors";
import { WinnersBoardContainer, WinnersHeader, WinnersRow } from "./styled";

const WinnersBoard = () => {
  const { winners } = useSelector(getGameData);
  return (
    <WinnersBoardContainer>
      <WinnersHeader>WINNERS</WinnersHeader>
      {winners.map(winner => (
        <WinnersRow key={`winner-${Math.random()}`}>
          {winner.username} won <span>$ {winner.sum}</span>
        </WinnersRow>
      ))}
    </WinnersBoardContainer>
  );
};

export default WinnersBoard;
