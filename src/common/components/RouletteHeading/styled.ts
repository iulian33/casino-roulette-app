import styled, { css } from "styled-components";
import Roulette1 from "./../../../assets/roulette_1.jpg";
import Roulette2 from "./../../../assets/roulette_2.png";
import Roulette3 from "./../../../assets/roulette_3.png";
import Roulette4 from "./../../../assets/roulette_4.png";
import Roulette5 from "./../../../assets/roulette_5.png";
import { BlackNumbers } from "../../constants";

type LayerProps = { idx: number; isWheel?: boolean };
type HistoryItemProps = { item: number };

const wheelLayers: { [key: number]: any } = {
  2: Roulette2,
  3: Roulette3,
  4: Roulette4,
  5: Roulette5
};

export const RouletteWheel = styled.div`
  float: right;
  width: 380px;
  height: 380px;
  border-radius: 100%;
  background: url(${Roulette1});
  background-size: 380px 380px;
  shape-outside: circle(190px);
  margin: 0 0 1em 1em;
  box-shadow: 2px 10px 30px rgba(0, 0, 0, 0.4);
  position: relative;
  touch-action: none;
  overflow: visible;

  @media screen and (max-width: 2640px) {
    float: none;
    margin: 1em auto;
  }

  @media screen and (max-width: 375px) {
    float: none;
    left: 30px;

    body {
      padding: 0 20px;
    }
  }
`;

export const Layer = styled.div<LayerProps>`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-size: 380px 380px;
  background-image: url(${({ idx }) => idx !== 0 && wheelLayers[idx]});

  ${({ isWheel }) =>
    isWheel &&
    css`
      will-change: transform;
    `}

  svg {
    position: absolute;
    top: 0;
    left: 0;
  }
`;

export const WheelBall = styled.div`
  position: absolute;
  width: 14px;
  height: 14px;
  border-radius: 7px;
  background: #fff radial-gradient(circle at 5px 5px, #fff, #444);
  box-shadow: 1px 1px 4px #000;
  transform: translateY(-116px);
  top: 50%;
  left: 50%;
  margin: -7px;
  will-change: transform;
`;

export const WinnersBoardContainer = styled.div`
  vertical-align: top;
  width: 80%;
  max-width: 350px;
  margin: 35px auto;
  padding: 15px 30px;
  height: 330px;
  border-radius: 10px;
  overflow: auto;
  background-color: rgba(0, 0, 0, 0.3);
`;

export const WinnersHeader = styled.div`
  text-align: center;
  font-size: 24px;
  font-weight: 600;
  margin-top: 20px;
  color: #f5f5dc;
`;

export const WinnersRow = styled.div`
  font-size: 18px;
  font-weight: 600;
  margin-top: 20px;
  color: #f5f5dc;
  overflow: auto;

  span {
    float: right;
  }
`;

export const WinHistoryContainer = styled.div`
  height: 100%;
  align-items: center;
  justify-content: center;
  display: inline-flex;
`;

export const History = styled.div`
  display: inline-block;
  width: 100%;
  max-width: 400px;
`;

export const HistoryItem = styled.div<HistoryItemProps>`
  float: left;
  color: #fff;
  font-weight: 900;
  line-height: 40px;
  margin-left: 20px;
  margin-top: 10px;
  width: 40px;
  height: 40px;
  margin-bottom: 10px;
  text-align: center;
  background-color: ${({ item }) =>
    item === 0
      ? "rgb(42, 174, 42)"
      : BlackNumbers.includes(item)
      ? "rgb(41, 40, 40)"
      : "rgb(177, 6, 6)"};
`;
