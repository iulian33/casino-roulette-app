import styled, { css } from "styled-components";
import BlackChip from "./../../../assets/chip_black.png";
import BlueChip from "./../../../assets/chip_blue.png";
import OrangeChip from "./../../../assets/chip_orange.png";
import PurpleChip from "./../../../assets/chip_purple.png";

type BoardChipProps = { selected: number | null; chipValue: number };
const handleChipBackground = (chip: number) => {
  switch (chip) {
    case 100:
      return `url('${BlackChip}') no-repeat`;
    case 20:
      return `url('${BlueChip}') no-repeat`;
    case 10:
      return `url('${OrangeChip}') no-repeat`;
    case 5:
      return `url('${PurpleChip}') no-repeat`;
    default:
      return `url('${BlackChip}') no-repeat`;
  }
};

export const RouletteActions = styled.div`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  position: relative;
  height: 150px;
  width: 100%;

  li {
    display: inline-flex;
    align-items: center;
    justify-content: center;
  }
`;

export const ButtonContainer = styled.li`
  margin: 0 20px;
`;

export const BoardChip = styled.li<BoardChipProps>`
  list-style: none;
  margin: 0 15px;
  display: block;

  div {
    position: relative;
    display: block;
    width: 60px;
    height: 60px;
    text-align: center;
    line-height: 63px;
    border-radius: 50%;
    font-size: 18px;
    color: #ffff;
    transition: 0.5s;
    background: ${({ chipValue }) => handleChipBackground(chipValue)};
    background-size: 60px 60px;

    ${({ selected, chipValue }) =>
      selected === chipValue &&
      css`
        color: #ffee10;
        box-shadow: 0 0 20px #ffee10;
        text-shadow: 0 0 5px #ffee10;
      `}

    &:before {
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      border-radius: 50%;
      transition: 0.5s;
      transform: scale(0.9);
      z-index: -1;
    }

    &:hover {
      color: #ffee10;
      box-shadow: 0 0 5px #ffee10;
      text-shadow: 0 0 5px #ffee10;

      &:before {
        transform: scale(1.1);
        box-shadow: 0 0 15px #ffee10;
      }
    }
  }

  &:before {
    content: "";
  }
`;
