import React, { useEffect } from "react";
import { Socket } from "socket.io-client";
import { useDispatch, useSelector } from "react-redux";
import { clearChips, updateSelectedChip } from "../../../store/slices/chipsData";
import { getChipsData, getGameData } from "../../../store/selectors";
import { setBetsConfirmed } from "../../../store/slices/gameData";
import { getChipClasses } from "../../utils/getChipClasses";
import { ChipValues, GameStages } from "../../constants";
import { Button, styled } from "@mui/material";
import { PlacedChip } from "../../models/Chip";
import { BoardChip, ButtonContainer, RouletteActions } from "./styled";

type BettingActionsProps = {
  socketServer?: Socket | null;
};

const BettingActions = ({ socketServer }: BettingActionsProps) => {
  const dispatch = useDispatch();
  const { placedChips, selectedChip } = useSelector(getChipsData);
  const { stage, betsConfirmed } = useSelector(getGameData);

  useEffect(() => {
    stage === GameStages.WINNERS && dispatch(setBetsConfirmed(false));
  }, [stage]);

  const placeBet = () => {
    let chips: PlacedChip[] = [];
    for (let key of Array.from(placedChips.keys())) {
      let chipsPlaced = placedChips.get(key) as PlacedChip;
      chips.push(chipsPlaced);
    }
    dispatch(setBetsConfirmed(true));
    socketServer?.emit("place-bet", JSON.stringify(chips));
  };

  const clearBet = () => {
    dispatch(clearChips());
    dispatch(setBetsConfirmed(false));
  };

  const ConfirmBets = styled(Button)({
    "&.MuiButton-root.Mui-disabled": {
      background: "rgba(44,118,21,.6)"
    }
  });

  return (
    <RouletteActions>
      <ul>
        <ButtonContainer>
          <Button variant="contained" color="error" onClick={clearBet} size="large">
            Clear Bet
          </Button>
        </ButtonContainer>

        {ChipValues.map(chipValue => (
          <BoardChip chipValue={chipValue} selected={selectedChip}>
            <div
              key={`chip_${chipValue}`}
              className={getChipClasses(chipValue, false)}
              onClick={() => !!chipValue && dispatch(updateSelectedChip(chipValue))}
            >
              {chipValue}
            </div>
          </BoardChip>
        ))}
        <ButtonContainer>
          <ConfirmBets
            variant="contained"
            disabled={betsConfirmed || stage !== GameStages.PLACE_BET}
            color="success"
            onClick={placeBet}
            size="large"
          >
            Place Bet
          </ConfirmBets>
        </ButtonContainer>
      </ul>
    </RouletteActions>
  );
};

export default BettingActions;
