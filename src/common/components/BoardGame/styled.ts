import styled, { css, keyframes } from "styled-components";
import Board from "./../../../assets/Board.png";
import { GameStages } from "../../constants";

type RouletteBoardProps = { stage: GameStages };

const boardRotateIn = keyframes`
  from { transform: rotateX(0deg) rotateY(0deg); }
  to { transform: rotateX(56deg) rotateY(0deg); }
`;

const boardRotateOut = keyframes`
  from { transform: rotateX(56deg) rotateY(0deg); } 
  to { transform: rotateX(0deg) rotateY(0deg); }
`;

export const RouletteBoardWrapper = styled.div`
  padding-top: 25px;
  perspective: 800px;
`;

export const RouletteBoard = styled.div<RouletteBoardProps>`
  float: none;
  display: flow-root;
  width: 800px;
  background-image: url(${Board});
  background-size: 100%;
  background-repeat: no-repeat;
  margin: 0 auto;
  transform-style: preserve-3d;
  animation: ${({ stage }) => css`
    ${stage === GameStages.PLACE_BET ? boardRotateOut : boardRotateIn} .6s ease forwards
  `};
`;

export const BoardGridNumbers = styled.div`
  display: grid;
  width: auto;
  height: 170px;
  margin: 22px 23px 17px 25px;
`;
export const BoardGridOthers = styled.div`
  display: grid;
  width: auto;
  height: 110px;
  margin: 22px 59px 17px 69px;
`;
