import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { CellItem } from "../../models/Game";
import { ChipContainer } from "../ChipContainer";
import { getNumbersList } from "../../utils/getNumbersList";
import { getChipsData, getGameData } from "../../../store/selectors";
import { getClassNamesFromCellItemType } from "../../utils/getClassNamesFromCellItemType";
import {
  Other_19_36,
  Other_1_12,
  Other_1_18,
  Other_2_12,
  Other_3_12,
  Other_black,
  Other_even,
  Other_odd,
  Other_red,
  ValueType
} from "../../constants";
import { PlacedChip } from "../../models/Chip";
import { BoardGridNumbers, BoardGridOthers, RouletteBoard, RouletteBoardWrapper } from "./styled";

export const BoardGame = () => {
  const { placedChips } = useSelector(getChipsData);
  const { stage } = useSelector(getGameData);
  const [numbers, setNumbers] = useState([] as CellItem[][]);

  const renderCell = (
    cell: CellItem,
    cellClass: string,
    currentItemChips: PlacedChip,
    rowSpan: number,
    colSpan: number
  ) => {
    const { type, value, valueSplit } = cell;
    const chipKey = `${type}_${value || `split_${valueSplit}`}`;

    return (
      <ChipContainer
        key={`cell__${Math.random()}`}
        currentItemChips={currentItemChips}
        tdKey={`td_${chipKey}`}
        chipKey={chipKey}
        cell={cell}
        rowSpan={rowSpan}
        colSpan={colSpan}
        cellClass={cellClass}
      />
    );
  };

  const renderEmptyCell = (keyId: number, cellClass: string) => {
    return <td key={`empty_${keyId}`} className={cellClass}></td>;
  };

  const renderOtherCell = (
    keyBase: string,
    cell: CellItem,
    currentItemChips: PlacedChip,
    rowSpan: number,
    colSpan: number
  ) => (
    <ChipContainer
      currentItemChips={currentItemChips}
      tdKey={`td_other_`}
      chipKey={`chip_other_${keyBase}`}
      cell={cell}
      rowSpan={rowSpan}
      colSpan={colSpan}
      cellClass={getClassNamesFromCellItemType(cell.type, null)}
    />
  );

  const renderNumberCell = (item: CellItem[], index: number) => {
    let keyId = 0;
    return (
      <tr key={`tr_board_${index}`}>
        {item.map((cell: CellItem) => {
          const cellClass = getClassNamesFromCellItemType(cell.type, cell.value);
          if (cell.type === ValueType.NUMBER && cell.value === 0) {
            const currentItemChips = placedChips.get(cell);
            return renderCell(cell, cellClass, currentItemChips!, 5, 1);
          } else {
            if (cell.type === ValueType.EMPTY) {
              keyId++;
              return renderEmptyCell(keyId, cellClass);
            } else {
              const currentItemChips = placedChips.get(cell);
              return renderCell(cell, cellClass, currentItemChips!, 1, 1);
            }
          }
        })}
      </tr>
    );
  };

  let currentItemChips_1_12 = placedChips.get(Other_1_12);
  let currentItemChips_2_12 = placedChips.get(Other_2_12);
  let currentItemChips_3_12 = placedChips.get(Other_3_12);
  let currentItemChips_1_18 = placedChips.get(Other_1_18);
  let currentItemChips_even = placedChips.get(Other_even);
  let currentItemChips_red = placedChips.get(Other_red);
  let currentItemChips_black = placedChips.get(Other_black);
  let currentItemChips_odd = placedChips.get(Other_odd);
  let currentItemChips_19_36 = placedChips.get(Other_19_36);

  useEffect(() => {
    setNumbers(getNumbersList());
  }, []);

  return (
    <RouletteBoardWrapper>
      <RouletteBoard stage={stage}>
        <BoardGridNumbers>
          <table>
            <tbody>{numbers.map((item, index) => renderNumberCell(item, index))}</tbody>
          </table>
        </BoardGridNumbers>
        <BoardGridOthers>
          <table>
            <tbody>
              <tr>
                {renderOtherCell("1_12", Other_1_12, currentItemChips_1_12!, 1, 11)}
                {renderOtherCell("2_12", Other_2_12, currentItemChips_2_12!, 1, 6)}
                {renderOtherCell("3_12", Other_3_12, currentItemChips_3_12!, 1, 10)}
              </tr>
              <tr>
                {renderOtherCell("1_18", Other_1_18, currentItemChips_1_18!, 1, 7)}
                {renderOtherCell("even", Other_even, currentItemChips_even!, 1, 3)}
                {renderOtherCell("red", Other_red, currentItemChips_red!, 1, 3)}
                {renderOtherCell("black", Other_black, currentItemChips_black!, 1, 3)}
                {renderOtherCell("odd", Other_odd, currentItemChips_odd!, 1, 3)}
                {renderOtherCell("19_36", Other_19_36, currentItemChips_19_36!, 1, 3)}
              </tr>
            </tbody>
          </table>
        </BoardGridOthers>
      </RouletteBoard>
    </RouletteBoardWrapper>
  );
};
