import classNames from "classnames";
import React, { memo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updatePlacedChips } from "../../../store/slices/chipsData";
import { getGameData } from "../../../store/selectors";
import { CellItem } from "../../models/Game";
import { PlacedChip } from "../../models/Chip";
import { GameStages } from "../../constants";
import { Chip } from "../Chip";
import { useBetFormatter } from "../../hooks/useBetFormatter";
import { ChipSum, ChipWrapper } from "./styled";

type ChipContainerProps = {
  currentItemChips: PlacedChip | undefined;
  tdKey: string;
  cellClass: string;
  chipKey: string;
  cell: CellItem;
  rowSpan: number;
  colSpan: number;
};
export const ChipContainer = memo(
  ({ tdKey, cellClass, chipKey, rowSpan, colSpan, cell, currentItemChips }: ChipContainerProps) => {
    const dispatch = useDispatch();
    const chipValue = useBetFormatter(currentItemChips?.sum || 0);
    const { betsConfirmed, stage } = useSelector(getGameData);

    const onPlaceChip = () => {
      !betsConfirmed && stage === GameStages.PLACE_BET && dispatch(updatePlacedChips(cell));
    };

    return (
      <td
        key={tdKey}
        className={classNames(cellClass, "chip-cell")}
        rowSpan={rowSpan}
        colSpan={colSpan}
        onClick={onPlaceChip}
      >
        <ChipWrapper>
          {!!currentItemChips && (
            <>
              <Chip key={chipKey} currentItemChips={currentItemChips} currentItem={cell} />
              <ChipSum>{chipValue}</ChipSum>
            </>
          )}
        </ChipWrapper>
      </td>
    );
  }
);
