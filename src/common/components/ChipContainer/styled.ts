import styled from "styled-components";

export const ChipWrapper = styled.div`
  position: relative;
  display: inline-block;
  width: 24px;
  height: 24px;
  left: 50%;
  transform: translateX(-50%);
`;
export const ChipSum = styled.div`
  position: absolute;
  font-weight: 900;
  font-size: 14px;
  top: 7px;
  -webkit-text-stroke: 1px blue;
  -webkit-text-fill-color: white;
  z-index: 10;
  left: 50%;
  transform: translateX(-50%);
`;
