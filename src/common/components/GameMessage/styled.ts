import styled from "styled-components";

export const RoundMessage = styled.div`
  text-align: center;
  margin: 20px auto 0;
  font-size: 24px;
  font-weight: 900;
  color: #fff;
`;
