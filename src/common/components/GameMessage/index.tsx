import React from "react";
import { GameStages } from "../../constants";
import { useSelector } from "react-redux";
import { getGameData } from "../../../store/selectors";
import { RoundMessage } from "./styled";

export const GameMessage = () => {
  const { stage } = useSelector(getGameData);
  return (
    <RoundMessage>
      {stage === GameStages.PLACE_BET
        ? "PLACE YOUR BETS"
        : stage === GameStages.WINNERS
        ? " SHOWING WINNERS"
        : "NO MORE BETS"}
    </RoundMessage>
  );
};
