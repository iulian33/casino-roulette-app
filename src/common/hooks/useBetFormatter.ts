import { useEffect, useState } from "react";

export const useBetFormatter = (bidAmount: number) => {
  const [formattedBidAmount, setFormattedBidAmount] = useState("");

  useEffect(() => {
    if (bidAmount < 1000) {
      setFormattedBidAmount(`${bidAmount}`);
    } else if (bidAmount < 10000) {
      setFormattedBidAmount(`${(bidAmount / 1000).toFixed(1)}K`);
    } else {
      setFormattedBidAmount(`${Math.floor(bidAmount / 1000)}K`);
    }
  }, [bidAmount]);

  return formattedBidAmount;
};
