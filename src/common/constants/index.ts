import { CellItem } from "../models/Game";

export enum ValueType {
  NUMBER,
  NUMBERS_1_12,
  NUMBERS_2_12,
  NUMBERS_3_12,
  NUMBERS_1_18,
  NUMBERS_19_36,
  EVEN,
  ODD,
  RED,
  BLACK,
  DOUBLE_SPLIT,
  QUAD_SPLIT,
  TRIPLE_SPLIT,
  EMPTY
}

export enum GameStages {
  PLACE_BET,
  NO_MORE_BETS,
  WINNERS,
  NONE
}

export const TotalTimeRouletteStage = 28;
export const SpinAnimationDuration = 5000;
export const SpinAnimationBezier = [0.165, 0.84, 0.44, 1.005];
export const ChipValues = [100, 20, 10, 5];
export const TotalGameNumbers = 37;
export const RotationDegree = 360 / TotalGameNumbers;

export const Other_1_12 = { type: ValueType.NUMBERS_1_12, valueSplit: [0] } as CellItem;
export const Other_2_12 = { type: ValueType.NUMBERS_2_12 } as CellItem;
export const Other_3_12 = { type: ValueType.NUMBERS_3_12 } as CellItem;
export const Other_1_18 = { type: ValueType.NUMBERS_1_18 } as CellItem;
export const Other_19_36 = { type: ValueType.NUMBERS_19_36 } as CellItem;
export const Other_even = { type: ValueType.EVEN } as CellItem;
export const Other_odd = { type: ValueType.ODD } as CellItem;
export const Other_red = { type: ValueType.RED } as CellItem;
export const Other_black = { type: ValueType.BLACK } as CellItem;

export const RouletteWheelNumbers = [
  0, 32, 15, 19, 4, 21, 2, 25, 17, 34, 6, 27, 13, 36, 11, 30, 8, 23, 10, 5, 24, 16, 33, 1, 20, 14,
  31, 9, 22, 18, 29, 7, 28, 12, 35, 3, 26
];

export const BlackNumbers = [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 29, 28, 31, 33, 35];

export const RedNumbers = [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36];
