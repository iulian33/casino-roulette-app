# Casino Roulette App



## About Project

Small multiplayer Roulette Casino Game that allows 2 and more users to play Roulette game 


## Run Project Locally 

- [ ] Install all packages by running `yarn` command 
- [ ] Run start script from `package.json` by running `yarn start` command that will run client side and server side simultaneously


## Stack of technology being used

- [ ] Frontend: `React`, `Typescript`, `styled-components`, `Material UI`, `Redux`, `Redux Toolkit`
- [ ] Backend: created small server on JavaScript by using Socket.io Server

Make sure to use a stable and mentinaced version of node.js 
