import { createServer } from "http";
import { Server } from "socket.io";
import {
  BlackNumbers,
  GameStages,
  RedNumbers,
  TotalTimeRouletteStage,
  ValueType
} from "../src/common/constants";
import { ServerGameData, Winner } from "../src/common/models/Game";
import { PlacedChip } from "../src/common/models/Chip";

/** Server Handling */
const httpServer = createServer();

const io = new Server(httpServer, {
  cors: {
    origin: "http://localhost:3000"
  }
});

let seconds = 0;
const users = new Map<string, string>();
const gameData = {} as ServerGameData;
const usersData = new Map<string, PlacedChip[]>();
const wins: Winner[] = [];

const rouletteTimerEngine = () => {
  seconds++;
  switch (seconds) {
    case 1:
      console.log("Placing bets step !");
      usersData.clear();
      gameData.stage = GameStages.PLACE_BET;
      sendGameUpdateEvent(gameData);
      break;

    case 20:
      console.log("No more bets step !");
      gameData.stage = GameStages.NO_MORE_BETS;
      gameData.rouletteWinValue = getRandomRouletteNumber(0, 36);
      sendGameUpdateEvent(gameData);

      for (let key of Array.from(usersData.keys())) {
        const username = users.get(key);
        if (username != undefined) {
          const chipsPlaced = usersData.get(key) as PlacedChip[];
          const sumWon = calculateWinnings(gameData.rouletteWinValue, chipsPlaced);
          wins.push({
            username: username,
            sum: sumWon
          });
        }
      }
      break;
    case 25:
      console.log("Winning step !");
      gameData.stage = GameStages.WINNERS;

      if (!gameData.history) {
        gameData.history = [];
      }

      gameData.history.push(gameData.rouletteWinValue);

      if (gameData.history.length > 10) {
        gameData.history.shift();
      }
      gameData.wins = wins.sort((a, b) => b.sum - a.sum);
      sendGameUpdateEvent(gameData);
      break;

    case TotalTimeRouletteStage:
      seconds = 0;
      break;
  }
};

io.on("connection", socket => {
  socket.on("enter", (data: string) => {
    users.set(socket.id, data);
    sendGameUpdateEvent(gameData);
  });

  socket.on("place-bet", (data: string) => {
    usersData.set(socket.id, JSON.parse(data));
  });
  socket.on("disconnect", reason => {
    users.delete(socket.id);
    usersData.delete(socket.id);
  });
});

httpServer.listen(8000, () => {
  console.log(`Server is running on port 8000`);
  setInterval(rouletteTimerEngine, 1000);
});

const getRandomRouletteNumber = (min: number, max: number) =>
  Math.floor(Math.random() * (max - min + 1)) + min;

const sendGameUpdateEvent = (gameData: ServerGameData) => {
  io.emit("game-update", JSON.stringify(gameData));
};

const calculateWinnings = (winningNumber: number, placedChips: PlacedChip[]) => {
  let win = 0;
  const chipLookup: { [key: string]: any } = {
    [ValueType.NUMBER]: (value: number) => value === winningNumber,
    [ValueType.BLACK]: (value: number) => BlackNumbers[value],
    [ValueType.RED]: (value: number) => RedNumbers[value],
    [ValueType.NUMBERS_1_18]: (value: number) => value >= 1 && value <= 18,
    [ValueType.NUMBERS_19_36]: (value: number) => value >= 19 && value <= 36,
    [ValueType.NUMBERS_1_12]: (value: number) => value >= 1 && value <= 12,
    [ValueType.NUMBERS_2_12]: (value: number) => value >= 13 && value <= 24,
    [ValueType.NUMBERS_3_12]: (value: number) => value >= 25 && value <= 36,
    [ValueType.EVEN]: (value: number) => value % 2 == 0,
    [ValueType.ODD]: (value: number) => value % 2 != 0,
    [ValueType.DOUBLE_SPLIT]: (value: number) => value / 2 != 0,
    [ValueType.TRIPLE_SPLIT]: (value: number) => value / 3 != 0,
    [ValueType.QUAD_SPLIT]: (value: number) => value / 4 != 0,
    [ValueType.EMPTY]: (value: number) => value === 0
  };

  for (let i = 0; i < placedChips.length; i++) {
    const { type, value } = placedChips[i].item;
    const { sum } = placedChips[i];
    const chipCheck = chipLookup[type](value);

    if (type === ValueType.NUMBER && chipCheck) {
      win += sum * 36;
    } else if (chipCheck) {
      win += sum * (type === ValueType.BLACK || type === ValueType.RED ? 2 : 3);
    }
  }

  return win;
};
